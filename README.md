## Introduction

Laboratory equipment is often expensive, even for simple apparatus. Our aim is to create an open-source syringe pump that can be 3D printed and used to operate microsyringes for applications in mass spectrometry and reaction sampling. This page containg a **detailed tutorial** of how to build such a pump under $150.

![Assembled syringe pump](/Images/assebbled_pump.png)

TBD - add a video of syringe pump in use

## Before the tutorial

To succesfully buils the pump, you will need:
- all the components listed in the folder "Components". It is possible to use similar parts, however then we cannot guarantee the advertized quality of the pump
- access to a 3D printer (alternatively it is possible to buy Ender 3v2 printer for reasonable price)
- soldering kit (or cables with end pins)
- licence for Autodesk Fusion 360 (free with student/academic email)

You can register for Autodesk account [here](https://accounts.autodesk.com/register), and download Fusion 360 [here](https://www.autodesk.com/products/fusion-360/overview) (only available for Windows and MacOS, or browser version). No knowledge of any software (including Fusion 360) is required to successfully build the pump.

To complete Part I of the tutorial, you will only need the access to the 3D printer and Autodesk Fusion 360 licence.

## Part I - building the pump

The first step is to decide what size of syringe are we bulding the pump for (after the pump is printed, this cannot be chnaged). Note down the following measurements of your syringe:

![Assembled syringe pump](/Images/parametrization.png)

Download the model of pump from the "Models" folder, and open it in Fusion 360. Once it is loaded, press **S** and a design shortcust window will pop up. Type "**change**" in it and click on "Change Parameters". Change the parameters d16, d17, d19, d21, d22, d24, d25, d39 and d40 to fit your syringe. Note that the metric used is milimeters.

There is an option to engrave the size of your syringe on the pump. This can be done as follows:
- in rigth menu (browser) unfold the sketches
- right click on sketch 20, and choose "Edit Sketch" option
- hover over the abreviation on the syringe auptil it turn dark blue
- right click on it and choose "Edit Text" option
- on the right side of the screen a pop-up window will apper, where there will be an option to edit text that is engraved on the pump
- after editing the text, press "OK" in the pop-up window and then "Finish Sketch" on the top right side of the screen

![Engraving](/Images/engraving.mp4 " ")

Now we want to export this modified model from Fusion 360. We will do this in two steps, first we will export the base of the pump, then the moving component of the pump. 
- Press "S" and type "3D print" into design shortcuts window that appeared. Select 3D Print, and a 3D print window will appear in the right side of the screen. Now in the Browser menu on the left side click on the **Component2:1**. Then in the 3D print window untick "Send to the 3D print ulility", press "OK" and save the file. 
- Press "S" and type "3D print" into design shortcuts window that appeared. Select 3D Print, and a 3D print window will appear in the right side of the screen. Now in the Browser menu on the left side click on the **Component3:1**. Then in the 3D print window untick "Send to the 3D print ulility", press "OK" and save the file. 

At this stage we have two .stl files that needs to be sliced to be 3D printed. This can be printer dependent, so this will be only a generic quide. We will use an Ultimate Cura slicer (can de dowloaded from [here](https://ultimaker.com/software/ultimaker-cura)) and Ender 3v2 printer. If your printer is calibrated correctly, you do not need to prine **ANY** supports. The base of the pump component is quite large so it is advised to print is on diagonal of your printer bed. It is enough to set the infil to 20%.

![mama](/Images/cura.png "Slicer settings")

TBD - image mid print

It takes around 1h 45min to print the moving component and 7h to print the body of the pump.

## Part II - wiring

To complete the second part of the tutoral, you need to have the components listed in the folder "Components". 

<img src="/Images/components.jpeg" alt="drawing" width="400"/>
<img src="/Images/components2.png" alt="drawing" width="700"/>

We will first connect the screen to Arduino Uno, and test if it's working correctly. Then we will connect the stepper motor, and the last step will be to try it together. 

## Part III - software and operation

TBD

## Credits

Contributors include Lindsey, Mark, Mathias, Sara and Scott
