#include <LiquidCrystal.h>
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
int inPin = 7, ls1 = 8, ls2 = 13;
int val = 0, vals1 = 0, vals2 = 0;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
void setup(){
//  lcd.begin(16, 2);
//  lcd.print("hello, world!");
//  delay(2000);
//  lcd.clear();
  pinMode(inPin, INPUT);    // declare pushbutton as input
  pinMode(ls1, INPUT);
  pinMode(ls2, INPUT);
  lcd.begin(16, 2);
  lcd.print("ready");
  lcd.setCursor(0,0);
}
void loop(){
//  lcd.setCursor(0,0);
//  lcd.print("time : ");
//  lcd.print(millis() / 1000);
//  char second = 's';
//  lcd.print(second);
  val = digitalRead(inPin);  // read input value
  vals1 = digitalRead(ls1);
  vals2 = digitalRead(ls2);
  if (vals1 == HIGH || vals2 == HIGH){
    lcd.setCursor(0,0);
    lcd.print("stop  ");
  } else {
    if (val == HIGH) { // check if the input is HIGH (button released)
      lcd.setCursor(0,0);
      lcd.print("moving");
    } else {
      lcd.setCursor(0,0);
      lcd.print("ready ");
  }
    }
}
