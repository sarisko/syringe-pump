#include <Stepper.h>
int ls1 = 12, ls2 = 13;
const int stepsPerRevolution = 200;  // change this to fit the number of steps per revolution
// for your motor
int steps = 2;
int val_limit_sw_1 = 0, val_limit_sw_2 = 0;

// initialize the stepper library on pins 8 through 11:
Stepper myStepper(stepsPerRevolution, 8, 9, 10, 11);

void setup() {
  // set the speed at 60 rpm:
  myStepper.setSpeed(60);
  // initialize the serial port:
  Serial.begin(9600);
  pinMode(ls1, INPUT);
  pinMode(ls2, INPUT);
}

void loop() {
  val_limit_sw_1 = digitalRead(ls1);
  val_limit_sw_2 = digitalRead(ls2);
  if (val_limit_sw_1 == HIGH){
    steps = -2;
  }
  if (val_limit_sw_2 == HIGH){
    steps = 2;
  }
  myStepper.step(steps);
  delay(10);
}
